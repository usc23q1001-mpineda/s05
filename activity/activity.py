
from abc import ABC, abstractclassmethod


class Animal(ABC):
    
 @abstractclassmethod
 def eat(self,food):
  pass
    
 def make_sound(self):
  pass
        
class Dog(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age

    # Setters
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    #Getters
    def get_name(self):
        print(f"Name: {self._name}.")

    def get_breed(self):
        print(f"Breed {self._breed}.")

    def get_Age(self):
        print(f"Age: {self._Age}")

    # Implementation of Abstract Method:
    
    def eat(self,food):
        print(f"Eaten {food}")
        
    def make_sound(self):
        print(f"Arf! Arf! Arf!")
    
    #Call
    def call(self):
        print(f"Here Lukey!")



class Cat(Animal):
    def __init__(self,name,breed,age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age
        
     # Setters
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    #Getters
    def get_name(self):
        print(f"Name: {self._name}.")

    def get_breed(self):
        print(f"Breed {self._breed}.")

    def get_Age(self):
        print(f"Age: {self._Age}")
        
    #Implementation    
    def eat(self,food):
        print(f"Serve me {food}")
    
    def make_sound(make_sound):
        print(f"Meow! Meow! Meow!")
    
    def call(self):
        print(f"Cmon Vic!")
        



dog1 = Dog("Luke", "Golden Retriever", 5)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Vic", "British Shorthair", 4)
cat1.eat("Fish")
cat1.make_sound()
cat1.call()